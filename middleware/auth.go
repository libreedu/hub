// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// This package contains useful middleware functions.
package middleware

import (
	"database/sql"
	"fmt"
	"net/http"

	"codeberg.org/libreedu/common/util"
	"codeberg.org/libreedu/hub"
	"codeberg.org/libreedu/hub/models"
)

// Filters requests based on whether `cond(...)` and the provided auth
// token is valid. If either is false, it rejects the request.
func Auth(db *hub.DB, cond func(*http.Request, models.User) bool) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			authToken := r.Header.Get("Authorization")
			u, err := db.CheckAuthToken(authToken)
			if err != nil {
				switch err {
				case models.ErrAuthTokenInvalid, models.ErrAuthTokenExpired, sql.ErrNoRows:
					util.JSON(w, map[string]string{"error": "forbidden"}, 401)
				default:
					// FIXME: Better error codes are necessary, e.g. 401 in case
					// of completely unknown auth token.
					fmt.Println(err.Error())
					util.JSON(w, map[string]string{"error": "unknown error"}, 500)
				}
				return
			}
			if !cond(r, u) {
				util.JSON(w, map[string]string{"error": "forbidden"}, 403)
				return
			}
			h.ServeHTTP(w, r)
		})
	}
}

// Allows all requests with valid auth tokens; cond always returns true.
func AuthPlain(db *hub.DB) func(http.Handler) http.Handler {
	return Auth(db, func(*http.Request, models.User) bool { return true })
}
