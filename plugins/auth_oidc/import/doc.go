// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// (Incomplete) Plugin auth_oidc adds support for OpenID Connect
// Discovery and authentication. In the future, it will support Dynamic
// Client Registration for non-fascist platforms.
//
// This plugin's config file is written in JSON. It has these keys:
//
//	"baseURL" // the URL of your Hub server
//	"fascistPlatforms" // a map of OIDC Provider URL to { "clientID", "clientSecret" }
//
// This currently assumes that provider URLs will always be equal to the
// issuer value in the Discovery document (which is what the spec says).
// However this can result in some misconfigured servers not working.
// The plugin also requires https support for OpenID Providers.
package auth_oidc
