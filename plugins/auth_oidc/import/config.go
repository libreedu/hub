// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package auth_oidc

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/url"

	"github.com/PuerkitoBio/purell"
	oidc "github.com/coreos/go-oidc/v3/oidc"
	jsoniter "github.com/json-iterator/go"
)

type configProvider struct {
	ClientID     string `json:"clientID"`
	ClientSecret string `json:"clientSecret"`
}
type configT struct {
	BaseURL          string                    `json:"baseURL"`
	FascistPlatforms map[string]configProvider `json:"fascistPlatforms"`
}

func (c configT) GetFascistPlatforms() (map[string]provider, []error) {
	providers := make(map[string]provider, len(c.FascistPlatforms))
	errs := make([]error, 0, len(c.FascistPlatforms))
	for k, pt := range c.FascistPlatforms {
		var kobj *url.URL
		if _kobj, err := url.Parse(k); err != nil {
			errs = append(errs, fmt.Errorf("invalid provider URL `%s`: %w", k, err))
			continue
		} else {
			kobj = _kobj
		}
		P := provider{
			clientID:     pt.ClientID,
			clientSecret: pt.ClientSecret,
		}
		if _P, err := oidc.NewProvider(context.TODO(), k); err != nil {
			errs = append(errs, fmt.Errorf("failed to discover provider %s: %w", k, err))
			continue
		} else {
			P.Provider = _P
			P.IDTokenVerifier = P.Verifier(&oidc.Config{
				ClientID: pt.ClientID,
			})
			providers[purell.NormalizeURL(kobj, normalizeFlags)] = P
		}
	}
	return providers, errs
}
func (c configT) MustBaseURL() *url.URL {
	url, err := url.Parse(c.BaseURL)
	if err != nil {
		panic(fmt.Sprintf("(unreachable) configured base URL did not parse! %s", err.Error()))
	}
	return url
}

func (p *plugin) readConfig(ch io.ReadSeekCloser) (cfg configT, _err error) {
	if ch == nil {
		return cfg, errors.New("no config provided!")
	}
	file, err := io.ReadAll(ch)
	if err != nil {
		return cfg, err
	}
	if err := jsoniter.Unmarshal(file, &cfg); err != nil {
		return cfg, err
	}
	_, _err = url.Parse(cfg.BaseURL)
	return cfg, _err
}
