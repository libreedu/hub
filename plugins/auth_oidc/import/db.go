// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package auth_oidc

import (
	"codeberg.org/libreedu/hub"
	"github.com/uptrace/bun"
)

func (p *plugin) Migrations() map[uint]func(*hub.DB) error {
	return map[uint]func(*hub.DB) error{
		0: func(db *hub.DB) error {
			_, err := db.Query(`
        create schema if not exists auth;
        create table auth.oidc (
          uid uuid primary key references users(id),
          sub text not null,
          iss text not null
        );
        -- create table auth.oidc_state_cache (         -- TODO
        --  state char(12) primary key,
        --  iss text not null,
        --  expires timestamptz
        -- );
        -- create table auth.oidc_dynamic_providers ();
      `)
			return err
		},
	}
}

type dbAuthEntry struct {
	bun.BaseModel `bun:"table:auth.oidc"`
	UID           string `bun:",pk"`
	Subject       string `bun:"sub"`
	Issuer        string `bun:"iss"`
}

type dbAuthProvider struct { // TODO
	bun.BaseModel `bun:"table:auth.oidc_dynamic_providers"`
}
