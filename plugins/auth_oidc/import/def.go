// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package auth_oidc

import (
	"fmt"
	"strings"

	"github.com/PuerkitoBio/purell"
	oidc "github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/exp/maps"

	"codeberg.org/libreedu/common/log"
	"codeberg.org/libreedu/hub"
)

const normalizeFlags purell.NormalizationFlags = purell.FlagRemoveDefaultPort |
	purell.FlagDecodeDWORDHost | purell.FlagDecodeOctalHost | purell.FlagDecodeHexHost |
	purell.FlagRemoveUnnecessaryHostDots | purell.FlagRemoveDotSegments | purell.FlagRemoveDuplicateSlashes |
	purell.FlagUppercaseEscapes | purell.FlagDecodeUnnecessaryEscapes | purell.FlagEncodeNecessaryEscapes |
	purell.FlagSortQuery | purell.FlagAddTrailingSlash

type plugin struct {
	logger    *log.Logger
	params    hub.PluginParams
	config    configT
	providers map[string]provider
}

type provider struct {
	*oidc.Provider
	*oidc.IDTokenVerifier
	clientID     string
	clientSecret string
}

func (p *plugin) Init(logger *log.Logger, params hub.PluginParams) error {
	p.logger, p.params, p.providers = logger, params, map[string]provider{}
	if config, err := p.readConfig(params.ConfigHandle); err != nil {
		return fmt.Errorf("failed to read in config: %w", err)
	} else {
		p.config = config
	}
	fproviders, errs := p.config.GetFascistPlatforms()
	p.providers = fproviders
	for _, err := range errs {
		logger.Warnf("failed to configure fascist platform: %s", err.Error())
	}
	logger.Infof("starting with providers: %s", strings.Join(maps.Keys(p.providers), ", "))

	return nil
}
func (p *plugin) Name() string { return "org.dachenedu.Hub.Auth.OpenIDConnect" }
func (p *plugin) Destroy() {
}

var (
	Plugin interface {
		hub.APIPlugin
		hub.DatabasePlugin
	} = &plugin{}
)
