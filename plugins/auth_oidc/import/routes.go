// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package auth_oidc

import (
	"io"
	"net/http"
	"net/url"

	"codeberg.org/libreedu/common/util"
	"github.com/PuerkitoBio/purell"
	"github.com/go-chi/chi/v5"
)

// TOOD: better OpenID authentication UI
const loginform = `<!doctype html>
<html>
<head><title>Sign in with OpenID Connect</title></head>
<body>
  <form action="/api/login/oidc" method="GET"><input required type="text" placeholder="Identity provider" aria-label="Identity provider" name="oidcaddr" /></form>
</body>
</html>
`

func (p *plugin) Route(r chi.Router) {
	r.Get("/login/oidc", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "text/html")
		io.WriteString(w, loginform)
	})
	r.Get("/api/login/oidc", func(w http.ResponseWriter, r *http.Request) {
		location, _ := url.Parse(r.URL.String())
		if _purl := r.URL.Query().Get("oidcaddr"); _purl == "" {
			goto back
		} else if purl, err := url.Parse(_purl); err != nil {
			goto back
		} else { // Keep variables in this scope to allow goto
			purl.Scheme = "https" // I stg if you dont have https in 2023
			var P provider
			if _P, ok := p.providers[purell.NormalizeURL(purl, normalizeFlags)]; ok {
				P = _P
			} else { // FIXME: Using non-fascist platforms is currently unsupported.
				p.logger.Debugf("unsupported OpenID platform %s requested", purl.String())
				goto back
			}
			//else if _P, err := oidc.NewProvider(context.TODO(), provider.String()); err == nil {
			//  ...
			//} else {
			//	p.logger.Errorf("failed to discover OIDC provider %s: %s", provider.String(), err.Error())
			//	goto back
			//}

			authurl, err := url.Parse(P.Endpoint().AuthURL)
			if err != nil {
				goto back
			}
			callback := p.config.MustBaseURL()
			callback.Path += "/api/login/oidc/callback"
			authurl.RawQuery = url.Values{
				"response_type": {"code"},
				"client_id":     {P.clientID},
				"redirect_uri":  {callback.String()},
				"state":         {util.RandSeq(12)},
			}.Encode()
			w.Header().Set("Location", authurl.String())
			w.WriteHeader(303)
			return
		}

	back:
		location.Path, location.RawQuery = "/login/oidc", ""
		w.Header().Add("Location", location.String())
		w.WriteHeader(303)
		return
	})

	r.Get("/api/login/oidc/callback", func(w http.ResponseWriter, r *http.Request) { // TODO
		w.WriteHeader(http.StatusNotImplemented)
	})
}
