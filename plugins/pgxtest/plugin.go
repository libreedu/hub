// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package pgxtest

import (
	"context"
	"io"
	"net/http"

	"codeberg.org/libreedu/common/log"
	"codeberg.org/libreedu/hub"
	resource "codeberg.org/libreedu/hub/exp/resource"
	"github.com/go-chi/chi/v5"
	"github.com/jackc/pgx/v5"
)

type plugin struct {
	logger *log.Logger
	params hub.PluginParams

	listen       *pgx.Conn
	listenCancel context.CancelFunc
}

func (p *plugin) Init(pl *log.Logger, pa hub.PluginParams) error {
	p.logger, p.params = pl, pa

	h := pa.Unstable.GetResHandle()
	listen, err := resource.GetDB[*pgx.Conn](h)
	if err != nil {
		return err
	}
	p.listen = listen

	go func() {
		ctx, cancel := context.WithCancel(context.Background())
		p.listenCancel = cancel

		_, err := p.listen.Exec(ctx, "listen mychan;")
		if err != nil {
			p.logger.Errorf("Error listening: %s", err.Error())
			cancel()
			return
		}
		for {
			select {
			case <-ctx.Done():
				p.listen.Close(context.Background())
				return
			default:
				notif, err := p.listen.WaitForNotification(ctx)
				if err != nil {
					if p.listen.IsClosed() {
						// Apparently pgconn does not wrap net.ErrClosed,
						// and instead creates its own error.
						p.logger.Errorf("Listen channel closed")
						return
					}
					p.logger.Errorf("Failed to wait for notification: %s", err.Error())
					continue
				}
				p.logger.Info() <- notif
				p.listen.Close(context.Background())
			}
		}
	}()

	return nil
}
func (p *plugin) Name() string { return "org.dachenedu.Hub.PGXTestPlugin" }
func (p *plugin) Route(r chi.Router) {
	r.Get("/api/notify", func(w http.ResponseWriter, r *http.Request) {
		payload := r.URL.Query().Get("payload")
		if payload == "" {
			payload = "example"
		}
		_, err := p.params.DB.Exec("notify mychan, ?;", payload)
		if err != nil {
			io.WriteString(w, err.Error())
		} else {
			io.WriteString(w, "Success")
		}
	})
}
func (p *plugin) Destroy() {
	p.listenCancel()
}

var (
	Plugin               = new(plugin)
	_      hub.APIPlugin = Plugin
)
