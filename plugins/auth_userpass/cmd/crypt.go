// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"fmt"
	"os"
	_ "unsafe"

	_ "codeberg.org/libreedu/hub/plugins/auth_userpass/import"
)

type passwordHash string
type passwordAlg string

//go:linkname crypt codeberg.org/libreedu/hub/plugins/auth_userpass/import.crypt
func crypt(password string) (passwordHash, passwordAlg, error)

func main() {
	if len(os.Args) < 3 || os.Args[1] != "crypt" {
		os.Exit(1)
	}
	fmt.Println(crypt(os.Args[2]))
}
