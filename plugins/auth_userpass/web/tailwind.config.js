// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

module.exports = { content: ['src/**/*.{tsx,jsx}', 'dist/**/*.html'] };
