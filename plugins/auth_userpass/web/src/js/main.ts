/*!
 * Copyright (c) 2022 Alexander Noble
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import { Login, redirect } from "./login.tsx";

(async () => {
  const token = sessionStorage.getItem("auth_token");
  let login = true;
  if (token != null) {
    let status: number;
    try {
      status = await fetch("/api/login/validate", {
        headers: { Authorization: token },
      }).then((r) => r.status);
    } catch {}
    login = status != 200;
  }

  // @ts-ignore: it could be null! but it won't be.
  if (login) document.getElementById("container").replaceChildren(Login());
  else redirect()
})();
