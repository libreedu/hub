// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

import {
  createElement,
  createFragment,
} from "https://libreedu.codeberg.page/components/@main/jsx.ts";

export function redirect() {
  const urlParams = new URLSearchParams(window.location.search)
  window.location.assign(urlParams.get("callback") ?? "/")
}

export function Login() {
  let [userInput, passInput, passShown, submit]: HTMLInputElement[] = [];
  let error: DocumentFragment;
  const form = (
    <form className="inline-block text-center" action="javascript:;">
      {userInput = (
        <input
          className="block border-gray-600 border-[1px] rounded-tl-lg rounded-tr-lg py-2 px-3 placeholder:italic placeholder:text-gray-600"
          type="text"
          placeholder="Username"
          aria-label="Username"
        />
      )}
      {passInput = (
        <input
          className="block border-gray-600 border-[1px] rounded-bl-lg rounded-br-lg py-2 px-3 placeholder:italic placeholder:text-gray-600 border-t-0"
          type="password"
          placeholder="Password"
          aria-label="Password"
        />
      )}
      <div className="my-2">
        {passShown = (
          <input className="mr-2" type="checkbox" id="password_shown_check" />
        )}
        <label htmlFor="password_shown_check">Show password</label>
      </div>
      {error = <></>}
      {submit = (
        <input
          className="py-2 px-3 text-white bg-fuchsia-700 rounded-lg"
          type="submit"
          value="Log in"
        >
        </input>
      )}
    </form>
  );
  passShown.addEventListener(
    "change",
    () => passInput.type = passShown.checked ? "text" : "password",
  );
  form.addEventListener("submit", async () => {
    let res: Response;
    try {
      res = await fetch("/api/login/password", {
        headers: {
          "content-type": "application/json",
        },
        body: JSON.stringify({
          uid: userInput.value,
          password: passInput.value,
        }),
        method: "POST",
      });
    } catch {}
    const json: { error?; authToken? } = await res.json();
    switch (res.status) {
      case 200:
        [userInput, passInput].forEach((e) =>
          e.setAttribute("data-invalid", "false")
        );
        error.replaceChildren(<></>);
        sessionStorage.setItem("auth_token", json.authToken);
        redirect();
        break;
      case 400:
      case 401:
      case 500:
        [userInput, passInput].forEach((e) =>
          e.setAttribute("data-invalid", "true")
        );
        error.replaceChildren(<p>Invalid username or password</p>);
        break;
    }
  });
  return form;
}
