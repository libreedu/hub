// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package embed

import (
	"testing"

	"github.com/alecthomas/assert"
)

func TestEmbed(t *testing.T) {
	f, err := staticFiles.Open(".files")
	assert.NoError(t, err, "embed is not embedding dist, or `go generate` is not building")
	f.Close()
}
