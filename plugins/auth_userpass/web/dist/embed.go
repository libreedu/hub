// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package embed

import "embed"

//go:embed *
var staticFiles embed.FS

func GetFiles() embed.FS {
	return staticFiles
}
