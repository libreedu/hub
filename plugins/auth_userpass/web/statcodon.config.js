// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

const config = {
  require: true,
  formats: {
    ts: {
      compile: async (t) => {
        const r = await require('esbuild').build({
          plugins: [require('@hyrious/esbuild-plugin-http').http()],
          jsxFactory: 'createElement',
          jsxFragment: 'createFragment',
          entryPoints: ['src/js/main.ts'],
          bundle: true,
          minify: true,
          legalComments: 'inline',
          sourcemap: true,
          target: ['chrome63', 'firefox57', 'safari12', 'edge42'],
          write: false,
        });
        return {
          text: r.outputFiles[0].text,
        };
      },
      minify: (t) => t,
      outExt: 'js',
    },
  },
  hooks: {
    finish: async () => {
      const fs = require('fs');
      const { logger } = require('statcodon/dist/util');
      const postcss = require('postcss')([
        require('tailwindcss')({}),
        require('autoprefixer')(),
        require('postcss-import')({ root: 'src/' }),
      ]);
      const cleancss = new (require('clean-css'))({});

      logger.currentFilename = 'src/main.css';
      logger.debug('Compiling file');
      const result = await postcss.process(
        fs.readFileSync('src/main.css', 'utf8')
      );
      result
        .warnings()
        .forEach((warn) => logger.warn('postcss:', warn.toString()));
      fs.writeFileSync(
        'dist/main.css',
        cleancss.minify(result.css).styles
      );
    },
  },
  ignore: ['src/js/!(main.ts)', 'src/**/*.css'],
};

module.exports = config;
