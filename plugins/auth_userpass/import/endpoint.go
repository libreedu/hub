// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package auth_userpass

import (
	"context"
	"net/http"

	"codeberg.org/libreedu/common/util"
	"github.com/google/uuid"
)

func login(w http.ResponseWriter, r *http.Request) {
	var cred struct {
		UID      string
		Password string
	}
	if !util.ParseJSON(w, r, &cred) {
		return
	}
	if cred.UID == "" || cred.Password == "" {
		util.JSON(w, map[string]string{"error": "expected \"uid\" and \"password\""}, 400)
		return
	}

	var uid string
	if _, err := uuid.Parse(cred.UID); err == nil {
		uid = cred.UID
	} else {
		params.DB.NewRaw("select id from users where username = ?;", cred.UID).Scan(context.Background(), &uid)
		// if uid == "", there was an error, and getUser below will fail.
	}

	u, err := getUser(uid)
	if err != nil {
		logger.Errorf("failed to get authuser %s: %s", cred.UID, err.Error())
		util.JSON(w, map[string]string{
			"error": "user did not exist or other error",
		}, 500)
		return
	}
	// FIXME: Ideally this would be handled by an ORM relation, but I
	// couldn't get that to work and not spit out nil User pointers.
	U, err := params.DB.GetUser(uid)
	if err != nil {
		logger.Errorf("failed to get user %s: %s", cred.UID, err.Error())
		util.JSON(w, map[string]string{
			"error": "user did not exist or other error",
		}, 500)
		return
	}

	if err := u.PasswordHash.Check(u.PasswordAlg, []byte(cred.Password)); err != nil {
		util.JSON(w, map[string]string{
			"error": "incorrect or invalid password",
		}, 401)
		return
	}

	// TODO: implement MFA support

	if token, err := params.DB.IssueAuthToken(U); err == nil {
		util.JSON(w, map[string]string{
			"authToken": token,
		}, 200)
		return
	} else {
		logger.Errorf("failed to issue auth token: %s", err.Error())
		util.JSON(w, map[string]string{
			"error": "failed to issue auth token",
		}, 500)
		return
	}
}

func mfa(w http.ResponseWriter, r *http.Request) {}
