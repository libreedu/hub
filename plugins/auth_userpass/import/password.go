// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package auth_userpass

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

type passwordHash string

// String encoding "+" intermediate hash "+" crypt hash
// e.g "base64+sha256+bcrypt"
type passwordAlg string

func crypt(password string) (passwordHash, passwordAlg, error) {
	v := sha256.Sum256([]byte(password))
	p, err := bcrypt.GenerateFromPassword(v[:], bcrypt.DefaultCost)
	return passwordHash(p), "plain+sha256+bcrypt", err
}

var encoding = map[string]func([]byte) ([]byte, error){
	"plain": func(h []byte) ([]byte, error) { return h, nil },
	"base64": func(h []byte) (d []byte, err error) {
		_, err = base64.RawStdEncoding.Decode(d, h)
		return
	},
	"base64url": func(h []byte) (d []byte, err error) {
		_, err = base64.RawURLEncoding.Decode(d, h)
		return
	},
}
var inthash = map[string]func([]byte) []byte{
	"plain":  func(b []byte) []byte { return b },
	"sha256": func(b []byte) []byte { h := sha256.Sum256(b); return h[:] },
}
var cryptmap = map[string]func(h []byte, c []byte) error{
	"plain": func(h, c []byte) error {
		if bytes.Compare(h, c) == 0 {
			return nil
		} else {
			return errors.New("plain password not equal")
		}
	},
	"bcrypt": func(h, c []byte) error { return bcrypt.CompareHashAndPassword(h, c) },
}

func (h passwordHash) Check(a passwordAlg, p []byte) error {
	if h == "" {
		return errors.New("no password")
	}
	W := []byte(h)
	// encoding "+" intermediate hash function "+" crypt function
	//
	// e.g. "base64url+sha256+bcrypt"
	var alg = strings.Split(string(a), "+")

	if encoding, ok := encoding[alg[0]]; ok {
		w, err := encoding(W)
		if err != nil {
			return err
		}
		W = w
	} else {
		return errors.New("unknown encoding")
	}

	if inthash, ok := inthash[alg[1]]; ok {
		p = inthash(p)
	} else {
		return errors.New("unknown intermediate hash algorithm")
	}

	if crypt, ok := cryptmap[alg[2]]; ok {
		return crypt(W, p)
	} else {
		return errors.New("unknown crypt algorithm")
	}
}
