// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package auth_userpass

import (
	"context"

	plugins "codeberg.org/libreedu/hub"
	"github.com/uptrace/bun"
)

func migrations() map[uint]func(*plugins.DB) error {
	return map[uint]func(*plugins.DB) error{
		0: func(db *plugins.DB) error {
			_, err := db.Query(`
        create schema if not exists auth;
        create table auth.userpass (
          uid uuid unique not null references public.users(id),
          password_hash text not null,
          password_alg text not null
          -- TODO: database MFA support
        );
      `)
			return err
		},
	}
}

type user struct {
	bun.BaseModel `bun:"table:auth.userpass"`
	UID           string `bun:",pk"`
	PasswordHash  passwordHash
	PasswordAlg   passwordAlg
}

func getUser(id string) (user, error) {
	ctx := context.Background()
	m := new(user)
	err := params.DB.NewSelect().Model((*user)(nil)).Where("uid = ?", id).Scan(ctx, m)
	return *m, err
}
