// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// This plugin adds support for logging in with username, password, and
// extensible other factors.
package auth_userpass

import (
	"net/http"

	embed "codeberg.org/libreedu/hub/plugins/auth_userpass/web/dist"

	"codeberg.org/libreedu/common/log"
	plugins "codeberg.org/libreedu/hub"
	"github.com/go-chi/chi/v5"
)

var logger *log.Logger
var params plugins.PluginParams

type examplePlugin struct{}

func (p examplePlugin) Init(pl *log.Logger, pa plugins.PluginParams) error {
	logger, params = pl, pa
	return nil
}
func (p examplePlugin) Name() string { return "org.dachenedu.Hub.Plugin.Auth.UserPassFlow" }
func (p examplePlugin) Route(r chi.Router) {
	r.Mount("/login/password", http.StripPrefix("/login/password/", http.FileServer(http.FS(embed.GetFiles()))))
	r.Post("/api/login/password", login)
}
func (p examplePlugin) Migrations() map[uint]func(*plugins.DB) error { return migrations() }
func (p examplePlugin) Destroy()                                     {}

var (
	Plugin examplePlugin
	_      plugins.APIPlugin      = Plugin
	_      plugins.DatabasePlugin = Plugin
)
