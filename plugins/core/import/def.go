// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package core

import (
	"net/http"

	"codeberg.org/libreedu/common/log"
	"codeberg.org/libreedu/common/util"
	plugins "codeberg.org/libreedu/hub"
	"github.com/go-chi/chi/v5"
)

var info struct {
	Version string   `json:"version"`
	Plugins []string `json:"plugins"`
}

type corePlugin struct {
	logger *log.Logger
	params plugins.PluginParams
}

func (p *corePlugin) Init(pl *log.Logger, pa plugins.PluginParams) error {
	p.logger, p.params = pl, pa
	return nil
}
func (p *corePlugin) Name() string { return "org.dachenedu.Hub.Core" }
func (p *corePlugin) Route(r chi.Router) {
	r.Get("/api/login/validate", func(w http.ResponseWriter, r *http.Request) {
		u, err := p.params.DB.CheckAuthToken(r.Header.Get("Authorization"))
		if err != nil {
			util.JSON(w, map[string]string{"error": "forbidden"}, 401)
			return
		}
		util.JSON(w, map[string]interface{}{"user": u}, 200)
	})
	r.Get("/api/icanhasbukkit", func(w http.ResponseWriter, r *http.Request) { util.JSON(w, info, 0) })
}
func (p *corePlugin) Extend(pm map[string]plugins.Plugin) error {
	info.Version = plugins.Version
	info.Plugins = []string{}
	for k := range pm {
		info.Plugins = append(info.Plugins, k)
	}
	return nil
}
func (p *corePlugin) Destroy() {}

var (
	Plugin                     = new(corePlugin)
	_      plugins.APIPlugin   = Plugin
	_      plugins.CrossPlugin = Plugin
)
