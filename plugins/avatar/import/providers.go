// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package avatar

import (
	"bytes"
	"context"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"io"
	"net/http"
	"net/url"
	"time"

	"codeberg.org/libreedu/hub"
	"github.com/cespare/xxhash/v2"
	minio "github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/uptrace/bun"
)

type avatarProvider interface {
	GetAvatar(ctx context.Context, uid string) (img []byte, mime string, err error)
	GetAvatarURL(context.Context, string) (string, error)
	PostAvatar(ctx context.Context, uid string, img []byte, mime string) error

	mustProxy() bool
}

func mustProxyURL(uid string) string {
	return "/api/avatar/" + uid
}

var errorAvatarNotExists = errors.New("avatar for user does not exist")
var errorNoDecoders = errors.New("there are no decoders available for the avatar")

func (p *avatarPlugin) newProvider(name string) (avatarProvider, error) {
	switch name {
	case "postgres":
		return &avatarPgProvider{
			db: p.params.DB,
		}, nil
	case "s3":
		client, err := minio.New(p.config.S3.Endpoint, &minio.Options{
			Creds:  credentials.NewStaticV4(p.config.S3.KeyID, p.config.S3.KeySecret, ""),
			Secure: true,
		})
		if err != nil {
			return nil, err
		}
		return &avatarS3Provider{
			db:     p.params.DB,
			client: client,
			bucket: p.config.S3.Bucket,
			cdn:    p.config.S3.cdn,
			proxy:  p.config.S3.Proxy,
		}, nil
	default:
		return nil, errors.New("no such avatar provider: " + name)
	}
}

// === Postgres ===

type avatarPgProvider struct {
	db *hub.DB
}
type avatarPg struct {
	bun.BaseModel `bun:"table:user_avatars_pg"`
	UID           string
	Avatar        []byte
	MediaType     string
}

func (pg *avatarPgProvider) GetAvatar(ctx context.Context, uid string) ([]byte, string, error) {
	var av avatarPg
	if err := pg.db.NewSelect().Model(&av).Where("uid = ?", uid).Scan(ctx, &av); err != nil {
		return nil, "", errorAvatarNotExists
	}
	return av.Avatar, av.MediaType, nil
}
func (*avatarPgProvider) GetAvatarURL(_ context.Context, uid string) (string, error) {
	return mustProxyURL(uid), nil
}
func (pg *avatarPgProvider) PostAvatar(ctx context.Context, uid string, img []byte, mime string) error {
	_, err := pg.db.NewInsert().
		Model(&avatarPg{
			UID:       uid,
			Avatar:    img,
			MediaType: mime,
		}).
		On("conflict (uid) do update").
		Set("avatar = excluded.avatar").
		Set("media_type = excluded.media_type").
		Exec(ctx)
	return err
}
func (pg *avatarPgProvider) mustProxy() bool { return true }

var _ avatarProvider = new(avatarPgProvider)

// === S3 ===

type avatarS3Provider struct {
	db     *hub.DB
	client *minio.Client
	bucket string
	cdn    *url.URL
	proxy  bool
}

func (s3 *avatarS3Provider) GetAvatar(ctx context.Context, uid string) ([]byte, string, error) {
	var hash string
	if err := s3.db.NewRaw("select hash from user_avatars_s3 where uid = ?;", uid).Scan(ctx, &hash); err != nil {
		return nil, "", errorAvatarNotExists
	}

	var r io.Reader
	var mime string
	if s3.cdn != nil {
		resp, err := http.Get(s3.cdn.JoinPath(hash).String())
		if err != nil {
			return nil, "", err
		}
		r, mime = resp.Body, resp.Header.Get("Content-Type")
	} else {
		obj, err := s3.client.GetObject(ctx, s3.bucket, hash, minio.GetObjectOptions{})
		if err != nil {
			return nil, "", err
		}
		stat, err := obj.Stat()
		if err != nil {
			return nil, "", err
		}
		r, mime = obj, stat.ContentType
	}
	img, err := io.ReadAll(r)
	return img, mime, err
}
func (s3 *avatarS3Provider) GetAvatarURL(ctx context.Context, uid string) (string, error) {
	if s3.proxy {
		return mustProxyURL(uid), nil
	}
	var hash string
	if err := s3.db.NewRaw("select hash from user_avatars_s3 where uid = ?;", uid).Scan(ctx, &hash); err != nil {
		return "", errorAvatarNotExists
	}

	if s3.cdn != nil {
		return s3.cdn.JoinPath(hash).String(), nil
	}

	url, err := s3.client.PresignedGetObject(ctx, s3.bucket, hash, 48*time.Hour, nil)
	return url.String(), err
}
func (s3 *avatarS3Provider) PostAvatar(ctx context.Context, uid string, img []byte, mime string) error {
	hashb := make([]byte, 8)
	binary.BigEndian.PutUint64(hashb, xxhash.Sum64(img))
	hash := hex.EncodeToString(hashb)

	r := bytes.NewReader(img)
	_, err := s3.client.PutObject(ctx, s3.bucket, hash, r, int64(len(img)),
		minio.PutObjectOptions{ContentType: mime})
	if err != nil {
		return err
	}

	if _, err := s3.db.NewRaw("insert into user_avatars_s3 (uid, hash) values (?, ?);", uid, hash).
		Exec(ctx); err != nil {
		return err
	}
	return nil
}
func (s3 *avatarS3Provider) mustProxy() bool { return s3.proxy }

var _ avatarProvider = new(avatarS3Provider)
