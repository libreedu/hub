// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package avatar

import (
	"testing"

	"github.com/alecthomas/assert"
)

func TestEmbed(t *testing.T) {
	assert.Equal(t, "<!--", page[0:4], "frontend.html is not embedding")
}
