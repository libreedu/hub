// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package avatar

import (
	"encoding/json"
	"io"
	"net/url"
)

type avatarPluginConfig struct {
	// "postgres" or "s3" - if postgres, avatars are stored as binary in db.
	Storage string
	S3      struct {
		KeyID     string
		KeySecret string
		Endpoint  string
		Bucket    string
		CDN       string `json:"cdn"`
		cdn       *url.URL
		Proxy     bool
	}
}

func (p *avatarPlugin) readConfig(ch io.ReadSeeker) (cfg avatarPluginConfig, _err error) {
	defaultCfg := avatarPluginConfig{
		Storage: "postgres",
	}

	if ch == nil {
		return defaultCfg, nil
	}
	file, err := io.ReadAll(ch)
	if err != nil {
		return cfg, err
	}
	if err := json.Unmarshal(file, &cfg); err != nil {
		return cfg, err
	}

	if cfg.S3.CDN != "" {
		url, err := url.Parse(cfg.S3.CDN)
		if err != nil {
			return cfg, err
		}
		cfg.S3.cdn = url
	}

	return cfg, _err
}
