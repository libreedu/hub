// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package internal

import "testing"

func TestClosestThumb(t *testing.T) {
	testCases := []struct{ Input, Expected int }{
		{15, 32},
		{51924, 512},
		{128, 128},
		{129, 256},
		{192, 256},
		{257, 512},
		{512, 512},
		{0, 512},
	}

	for _, tc := range testCases {
		got := ClosestThumb(tc.Input)
		if got != tc.Expected {
			t.Errorf("ClosestThumb(%d) == %d, expected %d", tc.Input, got, tc.Expected)
			t.Fail()
		}
	}
}
