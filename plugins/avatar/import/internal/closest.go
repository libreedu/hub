// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package internal

import (
	"unsafe"

	"golang.org/x/exp/constraints"
)

func intLog2[T constraints.Integer](x T) (T, bool) {
	if x <= 0 {
		return 0, false
	}
	size := int(unsafe.Sizeof(x)*8 - 1)
	if T(0)-1 < 0 {
		// Sign bit is not part of the number
		size--
	}
	for i := size; i > -1; i-- {
		y := x >> i
		if y&1 == 1 {
			return T(i), true
		}
	}
	// x > 0, therefore at least one bit is set
	panic("unreachable")
}

func ClosestThumb(size int) int {
	if size == 0 {
		return 512
	}
	s := 32
	for s < 512 && size > s {
		s <<= 1
	}
	return s
}
