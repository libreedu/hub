// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package internal

import (
	"image"
	"image/color"

	"github.com/google/uuid"
)

var gen = sigil{
	Rows: 5,
	Foreground: []color.NRGBA{
		{229, 137, 137, 255},
		{229, 229, 137, 255},
		{137, 229, 137, 255},
		{137, 229, 229, 255},
		{137, 137, 229, 255},
		{229, 137, 229, 255},
		{229, 229, 137, 255},
		{211, 137, 229, 255},
	},
	Background: color.NRGBA{224, 224, 224, 255},
}

func MakeIdenticon(uuidStr string, size int) (image.Image, error) {
	uuidObj, err := uuid.Parse(uuidStr)
	if err != nil {
		return nil, err
	}
	// cn := int(([16]byte(uuidObj))[15]) % 8
	return gen.Make(size, false, uuidObj[:]), nil
}
