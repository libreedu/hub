// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package avatar

import (
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	_ "unsafe"

	"golang.org/x/image/draw"
)

var scaler = draw.ApproxBiLinear
var rect = image.Rect

type decoder func(io.Reader) (image.Image, error)
type encoder func(io.Writer, image.Image) error

var (
	decoders = map[string]decoder{
		"image/jpeg": jpeg.Decode,
		"image/png":  png.Decode,
		"image/gif":  gif.Decode,
	}
	encoders = map[string]encoder{
		"image/jpeg": func(w io.Writer, m image.Image) error { return jpeg.Encode(w, m, &jpeg.Options{Quality: 60}) },
		"image/png":  png.Encode,
		"image/gif": func(w io.Writer, m image.Image) error {
			return gif.Encode(w, m, &gif.Options{NumColors: 224, Quantizer: nil, Drawer: nil})
		},
	}
)

func scale(src image.Image, rect image.Rectangle, scale draw.Scaler) image.Image {
	dst := image.NewRGBA(rect)
	scale.Scale(dst, rect, src, src.Bounds(), draw.Over, nil)
	return dst
}
