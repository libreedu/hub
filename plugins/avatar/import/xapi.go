// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package avatar

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"image"
	"net/http"

	"codeberg.org/libreedu/hub"
	"codeberg.org/libreedu/hub/plugins/avatar/import/internal"
	"github.com/patrickmn/go-cache"
)

func normalizeID(ctx context.Context, db *hub.DB, id string) (string, error) {
	if len(id) == 36 {
		return id, nil
	} else if len(id) <= 32 {
		err := db.
			NewRaw("select id from users where username = ?;", id).
			Scan(ctx, &id)
		return id, err
	} else {
		return "", errors.New("invalid username")
	}
}

func identiconResponse(w http.ResponseWriter, id string, size int) {
	w.WriteHeader(404)
	img, err := internal.MakeIdenticon(id, size)
	if err != nil {
		img = internal.MakeBlank(size)
	}
	encoders["image/jpeg"](w, img)
}

type cacheEntry struct {
	Image     []byte
	MediaType string
}

func (p *avatarPlugin) getAvatarCache(ctx context.Context, id string) ([]byte, string, error) {
	id, err := normalizeID(ctx, p.params.DB, id)
	if err != nil {
		return nil, "", err
	}
	av, mime, err := p.provider.GetAvatar(ctx, id)
	if err != nil {
		return nil, "", err
	}
	cacheId := fmt.Sprintf("%sd%d", id, 512)
	p.cache.Set(cacheId, cacheEntry{av, mime}, cache.DefaultExpiration)
	return av, mime, nil
}

func (p *avatarPlugin) XGetAvatar(ctx context.Context, id string) (image.Image, error) {
	av, mime, err := p.getAvatarCache(ctx, id)
	if err != nil {
		return nil, err
	}
	dec, decok := decoders[mime]
	if !decok {
		return nil, errorNoDecoders
	}
	return dec(bytes.NewReader(av))
}
func (p *avatarPlugin) XGetAvatarURL(ctx context.Context, id string) (string, error) {
	id, err := normalizeID(ctx, p.params.DB, id)
	if err != nil {
		return "", err
	}
	return p.provider.GetAvatarURL(ctx, id)
}
