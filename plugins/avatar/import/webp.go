// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

//go:build avatar_webp

package avatar

import (
	"image"
	"io"

	webp "github.com/kolesa-team/go-webp/encoder"
	xwebp "golang.org/x/image/webp"
)

func init() {
	opts, err := webp.NewLosslessEncoderOptions(webp.PresetDefault, 5)
	if err == nil {
		webpOpts = opts
		decoders["image/webp"] = webpDecode
		encoders["image/webp"] = webpEncode
	} else {
		logger.Errorf("failed to init webp encoder: %s", err.Error())
	}
}

var webpOpts *webp.Options

// Prefer the /x decoder
var webpDecode decoder = xwebp.Decode
var webpEncode encoder = func(w io.Writer, m image.Image) error {
	enc, err := webp.NewEncoder(m, webpOpts)
	if err != nil {
		return err
	}
	return enc.Encode(w)
}
