// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// FIXME: Whole plugin has very messy code.

// This plugin allows viewing and uploading user avatars.
// {GET, POST} /api/avatar/UID to {view, upload}
//
// To enable WebP support, ensure cgo is enabled and add the build flag
// `-tags avatar_webp`
package avatar

import (
	"bytes"
	"context"
	_ "embed"
	"fmt"
	"image"
	"io"
	"math"
	"net/http"
	"strconv"
	"time"

	"codeberg.org/libreedu/hub"
	"codeberg.org/libreedu/hub/middleware"
	"codeberg.org/libreedu/hub/models"
	"codeberg.org/libreedu/hub/plugins/avatar/import/internal"

	"codeberg.org/libreedu/common/log"
	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	cache "github.com/patrickmn/go-cache"
)

//go:embed frontend.html
var page string

type avatarPlugin struct {
	logger *log.Logger
	params hub.PluginParams
	config avatarPluginConfig

	provider avatarProvider
	cache    *cache.Cache
}

type AvatarPlugin interface {
	hub.APIPlugin
	hub.DatabasePlugin

	XGetAvatar(ctx context.Context, id string) (image.Image, error)
	XGetAvatarURL(ctx context.Context, id string) (string, error)
}

func (p *avatarPlugin) Init(pl *log.Logger, pa hub.PluginParams) (err error) {
	p.logger, p.params = pl, pa
	p.cache = cache.New(time.Hour, 30*time.Minute)
	if config, err := p.readConfig(p.params.ConfigHandle); err != nil {
		return fmt.Errorf("failed to read in config: %w", err)
	} else {
		p.config = config
	}

	p.provider, err = p.newProvider(p.config.Storage)
	if err != nil {
		return err
	}
	return err
}
func (p *avatarPlugin) Name() string { return "org.dachenedu.Hub.AvatarPlugin" }
func (p *avatarPlugin) Route(r chi.Router) {
	r.Get("/user/avatar", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("content-type", "text/html")
		io.WriteString(w, page)
	})
	r.Get("/api/avatar/{uid}", func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		size, _ := strconv.Atoi(r.URL.Query().Get("size"))
		size = internal.ClosestThumb(size)

		uid := chi.URLParam(r, "uid")
		if nid, err := normalizeID(ctx, p.params.DB, uid); err == nil {
			// We normalize ID because identicon only works on UUIDs, but
			// if not, it is not a big deal to return blank.
			uid = nid
		}

		// FIXME: This results in an avatar smaller than 512px being cached
		// multiple times because the dimensions of the avatar are unknown.
		// FIXME 2: is above still relevant?
		cacheId := fmt.Sprintf("%sd%d", uid, size)
		if av, ok := p.cache.Get(cacheId); ok {
			w.Header().Set("Content-Type", av.(cacheEntry).MediaType)
			w.Write(av.(cacheEntry).Image)
			return
		}

		if !p.provider.mustProxy() {
			// TODO: Scaling when using CDN
			url, err := p.XGetAvatarURL(ctx, uid)
			if err != nil {
				identiconResponse(w, uid, size)
				return
			}
			w.Header().Set("Location", url)
			w.WriteHeader(302)
			return
		}

		img, err := p.XGetAvatar(ctx, uid)
		if err != nil {
			identiconResponse(w, uid, size)
			return
		}
		if img.Bounds().Dx() > size {
			img = scale(img, rect(0, 0, size, size), scaler)
		}
		codec := "image/webp"
		enc, encok := encoders[codec]
		if !encok {
			codec = "image/jpeg"
			enc = encoders[codec]
		}
		buf := new(bytes.Buffer)
		if err := enc(buf, scale(img, rect(0, 0, size, size), scaler)); err != nil {
			w.WriteHeader(500)
			return
		}

		p.cache.Set(cacheId, cacheEntry{buf.Bytes(), codec}, cache.DefaultExpiration)
		w.Write(buf.Bytes())
	})

	r.With(middleware.Auth(p.params.DB, func(r *http.Request, u models.User) bool {
		id, err := uuid.Parse(chi.URLParam(r, "uid"))
		return err == nil && id.String() == uuid.MustParse(u.ID).String()
	})).Post("/api/avatar/{uid}", func(w http.ResponseWriter, r *http.Request) {
		var f io.Reader = r.Body
		var mime string = r.Header.Get("Content-Type")
		if mime == "multipart/form-data" {
			fo, fh, err := r.FormFile("photo")
			if err != nil {
				http.Error(w, `{"error":"server error"}`, 500)
				return
			}
			f = fo
			mime = fh.Header.Get("content-type")
		}
		dec, decok := decoders[mime]
		enc, encok := encoders[mime]
		if !decok || !encok {
			http.Error(w, `{"error":"unsupported image format"}`, 400)
			return
		}
		img, err := dec(f)
		if err != nil {
			http.Error(w, `{"error":"image failed to decode"}`, 400)
			return
		}

		d := int(math.Min(512, math.Min(float64(img.Bounds().Dx()), float64(img.Bounds().Dy()))))
		buf := new(bytes.Buffer)
		square := scale(img, rect(0, 0, d, d), scaler)
		if err := enc(buf, square); err != nil {
			http.Error(w, `{"error":"image failed to reencode"}`, 500)
			return
		}
		b := buf.Bytes()

		if err := p.provider.PostAvatar(r.Context(), chi.URLParam(r, "uid"), b, mime); err != nil {
			p.logger.Errorf("failed to upsert avatar: %s", err.Error())
		}
		w.Write([]byte("{}"))
	})
}
func (p *avatarPlugin) Migrations() map[uint]func(*hub.DB) error {
	return map[uint]func(*hub.DB) error{
		0: func(db *hub.DB) error {
			_, err := db.Query(`
      create table user_avatars (
        uid uuid primary key references users(id),
        avatar bytea not null,
        media_type text not null
      );
      `)
			return err
		},
		1717530287: func(db *hub.DB) error {
			_, err := db.Query(`
      alter table user_avatars rename to user_avatars_pg;
      create table user_avatars_s3 (
        uid uuid primary key references users(id),
        hash char(16) -- xxh64
      );
      `)
			return err
		},
	}
}
func (p *avatarPlugin) Destroy() {}

var Plugin AvatarPlugin = new(avatarPlugin)
