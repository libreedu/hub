// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// Use codeberg.org/libreedu/hub/plugins/avatar/import in launch script
package main

import plugin "codeberg.org/libreedu/hub/plugins/avatar/import"

var Plugin = plugin.Plugin
