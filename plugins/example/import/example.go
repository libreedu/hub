// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package example

import (
	"net/http"

	"codeberg.org/libreedu/common/log"
	"codeberg.org/libreedu/hub"
	"codeberg.org/libreedu/hub/middleware"
	"github.com/go-chi/chi/v5"
)

type examplePlugin struct {
	logger *log.Logger
	params hub.PluginParams
}

func (p *examplePlugin) Init(pl *log.Logger, pa hub.PluginParams) error {
	p.logger, p.params = pl, pa
	return nil
}
func (p *examplePlugin) Name() string { return "org.dachenedu.Hub.ExamplePlugin" }
func (p *examplePlugin) Route(r chi.Router) {
	r.Get("/api/example-plugin", func(w http.ResponseWriter, r *http.Request) { w.Write([]byte("Hello, world!")) })
	r.With(middleware.AuthPlain(p.params.DB)).Post("/api/example-plugin-auth",
		func(w http.ResponseWriter, r *http.Request) { w.Write([]byte("Hello, world!")) })
}
func (p *examplePlugin) Destroy() {}

var (
	Plugin               = &examplePlugin{}
	_      hub.APIPlugin = Plugin
)
