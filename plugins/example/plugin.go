// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// Use codeberg.org/libreedu/hub/plugins/example/import in launch script
package main

import example "codeberg.org/libreedu/hub/plugins/example/import"

var Plugin = example.Plugin
