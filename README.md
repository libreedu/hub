# hub

[![Status: In Development](./docs/badges/status.svg)](#)
[![Version: —](./docs/badges/version.svg)](https://codeberg.org/libreedu/hub/releases)
[![License: AGPLv3](./docs/badges/license.svg)](#)
[![Issues](./docs/badges/issues.svg)](https://codeberg.org/libreedu/hub/issues)

A plugin-based application framework, for one app or many! Use this as a
Nextcloud-like platform, or just to bootstrap your (AGPLv3) app.

## Usage

### For deployment

All you need is a launch script like below. It uhm, should be pretty
self-explanatory.

<details><summary>Expand</summary>

```go
package main

import (
    "os"
    "os/signal"
    "syscall"

    "codeberg.org/libreedu/common/log"
    "codeberg.org/libreedu/hub"

    auth_userpass "codeberg.org/libreedu/hub/plugins/auth_userpass/import"
    core "codeberg.org/libreedu/hub/plugins/core/import"
    example "codeberg.org/libreedu/hub/plugins/example/import"
)

func main() {
    logger := log.New(os.Stdout, "hub-dist")
    defer logger.Recover()(true)

    manager, err := hub.GetDefaultManager(logger)
    if err != nil {
        panic(err.Error())
    }
    manager.AddPlugins(core.Plugin, example.Plugin, auth_userpass.Plugin)
    manager.Finalize()
    go manager.Listen(nil)

    ch := make(chan os.Signal, 1)
    signal.Notify(ch)
    for {
        switch <-ch {
        case os.Interrupt, syscall.SIGTERM, syscall.SIGINT:
            logger.Infof("Received interrupt signal, exiting")
            manager.Shutdown()
            return
        }
    }
}
```

</details>

> While it is possible to use plugins distributed as ELF (see
> `Manager.AddPluginFile`), it is discouraged because the versions of
> Dachen Hub and _all of its dependencies_ must be precisely equal to
> those required by the plugin file.

There's a problem though: you can't use the web interfaces for some
plugins. Many plugins use compiled resources which you do not generally
want to store in version control. If you need any of those resources,
what you can do instead is
[use vendoring](https://go.dev/ref/mod#vendoring) for your module (run
`go mod vendor`), with a tool such as [modvendor](https://github.com/goware/modvendor) (`modvendor -copy="**/hubplugin.just **/embed/**/*.* **/web/**/*.*" -v`), and then run
`just -f path/to/hubplugin.just preflight` in each of the plugin
directories. In shell script, this can be done automatically with:

```sh
for file in $(find vendor -name hubplugin.just); do just -f "$file" preflight; done
```

So this might be a good Justfile:

```makefile
dist: vendor preflight
  mkdir -p bin
  go build -o ./dachen -trimpath -buildmode pie .

vendor:
  go mod vendor
  go run github.com/goware/modvendor@latest -copy="**/web/**/*.*" -v

preflight:
  for file in $(find vendor -name hubplugin.just); do just -f "$file" preflight; done
```

### For plugin development

For the most part, you can just follow the
[example plugin template](https://codeberg.org/libreedu/hub/src/branch/main/plugins/example/import/example.go).
If you want your plugin to be loadable as a file by itself, you can use
`-buildmode plugin` and a small file like this:

```go
package main
import plugin "codeberg.org/.../import-path"
var Plugin = plugin.Plugin
```

If you need to embed any compiled resources, without including them in
version control, you can write a
[Justfile](https://github.com/casey/just) in your module with the name
`hubplugin.just` which can be run by people with the simple build
scripts above. Then you can use `go:embed` directives like usual. (Note
that Justfiles _always_ run in their containing directory.)

Also, as above, if you want to embed non-Go files please don't include a
`.` in your directory names and put them all in a folder named `web` or
`embed`.
