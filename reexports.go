// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package hub

import "codeberg.org/libreedu/hub/internal/types"

type DB = types.DB

type PluginParams = types.PluginParams

// A plugin.
//
// There is an example plugin in the package
// codeberg.org/libreedu/hub/plugins/example which simply registers
// the endpoint `/api/example-plugin` and returns the string "Hello,
// world!"
type Plugin = types.Plugin

// A plugin with an API router.
type APIPlugin = types.APIPlugin

// A plugin that extends another plugin, or just has access to the
// plugin map.
type CrossPlugin = types.CrossPlugin

// A plugin with managed file-based SQL migrations. Migrations()
// returns a map of Unix timestamps to Go migrations. Any migrations
// since the last are each executed until one throws an error. On error,
// Destroy() is called, the database is restored, and an error is
// logged.
type DatabasePlugin = types.DatabasePlugin
