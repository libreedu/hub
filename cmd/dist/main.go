// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"os"
	"os/signal"
	"syscall"

	"codeberg.org/libreedu/common/log"
	"codeberg.org/libreedu/hub"
	auth_userpass "codeberg.org/libreedu/hub/plugins/auth_userpass/import"
	core "codeberg.org/libreedu/hub/plugins/core/import"
	example "codeberg.org/libreedu/hub/plugins/example/import"
)

func main() {
	logger := log.New(os.Stdout, "hub-dist")
	defer logger.Recover()(true)

	manager, err := hub.GetDefaultManager(logger)
	if err != nil {
		panic(err.Error())
	}
	manager.AddPlugins(core.Plugin, example.Plugin, auth_userpass.Plugin)
	manager.Finalize()
	go manager.Listen(nil)

	ch := make(chan os.Signal, 1)
	signal.Notify(ch)
	for {
		switch <-ch {
		case os.Interrupt, syscall.SIGTERM, syscall.SIGINT:
			logger.Infof("Received interrupt signal, exiting")
			manager.Shutdown()
			return
		}
	}
}
