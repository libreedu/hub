// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package types

import "github.com/jackc/pgx/v5"

type unstableAPI interface {
	GetPgx() (*pgx.Conn, error)
	GetResHandle() InternalHandle
}

type InternalHandle any
type internalHandle struct {
	m any
	p Plugin
}

func HandleFrom(m any, p Plugin) InternalHandle { return internalHandle{m: m, p: p} }

func (h internalHandle) Manager() any   { return h.m }
func (h internalHandle) Plugin() Plugin { return h.p }
