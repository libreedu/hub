// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package types

import (
	"context"

	models "codeberg.org/libreedu/hub/models"
	"github.com/uptrace/bun"
)

type DB struct{ *bun.DB }

func (db *DB) GetUser(id string) (models.User, error) {
	ctx := context.Background()
	m := new(models.User)
	err := db.NewSelect().Model((*models.User)(nil)).Where("id = ?", id).Scan(ctx, m)
	return *m, err
}

func (db *DB) GetAuthToken(token string) (models.AuthToken, error) {
	m := new(models.AuthToken)
	if err := db.NewSelect().Model((*models.AuthToken)(nil)).Where("token = ?", token).Scan(context.Background(), m); err != nil {
		return *m, err
	}
	// FIXME: ORM broke, now this has to manually scan in the User object.
	// Time to start writing the manifesto, "ORMs are evil"!
	m.User = new(models.User)
	err := db.NewSelect().Model((*models.User)(nil)).Where("id = ?", m.UID).Scan(context.Background(), m.User)
	return *m, err
}
