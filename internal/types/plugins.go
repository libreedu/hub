// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package types

import (
	"io"
	"io/fs"

	"codeberg.org/libreedu/common/log"
	"github.com/go-chi/chi/v5"
)

type PluginParams struct {
	// A struct containing a bun DB, which should be Postgres. If you
	// prefer, you can access a `*sql.DB` at `params.DB.DB.DB`.
	DB *DB

	// A file handle for a config file. If there is no configuration file,
	// this is set to nil. This is owned by the manager - the file will be
	// closed after the AddPlugins function (when Init is called) ends.
	ConfigHandle io.ReadSeekCloser

	Unstable unstableAPI
}

// A plugin.
//
// There is an example plugin in the package
// codeberg.org/libreedu/hub/plugins/example which simply registers
// the endpoint `/api/example-plugin` and returns the string "Hello,
// world!"
type Plugin interface {
	Init(*log.Logger, PluginParams) error
	// Returns the D-Bus or Java–style name for the plugin. Must not rely
	// on an earlier Init() call.
	Name() string
	// Function to close connections, end requests, etc.
	Destroy()
}

// A plugin with an API router.
type APIPlugin interface {
	Plugin
	// A router mounted on /. Generally, keep API extensions inside of
	// `/api/`; this interface gives / for well-known URIs, etc.
	Route(chi.Router)
}

// A plugin that extends another plugin, or just has access to the
// plugin map.
type CrossPlugin interface {
	Plugin
	// Called after all other plugins have been initialized.
	Extend(map[string]Plugin) error
}

// A plugin with managed file-based SQL migrations. Migrations()
// returns a map of Unix timestamps to Go migrations. Any migrations
// since the last are each executed until one throws an error. On error,
// Destroy() is called, the database is restored, and an error is
// logged.
type DatabasePlugin interface {
	Plugin
	Migrations() map[uint]func(*DB) error
}

// TODO: There should be a utility function converting a SQL embed FS to
// a bunch of functions. e.g.:
type _ func([]byte) func(*DB) error
type _ func(interface {
	fs.FS
	fs.ReadDirFS
	fs.ReadFileFS
}) map[uint]func(*DB) error

var _ interface {
	Plugin
	APIPlugin
	CrossPlugin
}
