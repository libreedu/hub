// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package types

import (
	"context"
	"time"

	"codeberg.org/libreedu/common/util"
	"codeberg.org/libreedu/hub/models"
)

func (db *DB) IssueAuthToken(u models.User) (token string, err error) {
	token = util.CryptoRandSeq(64)
	_, err = db.NewInsert().Model(&models.AuthToken{Token: token, UID: u.ID, User: &u, IssuedAt: time.Now()}).Exec(context.Background())
	if err != nil {
		return
	}
	u.LastLogin = time.Now()
	_, err = db.DB.NewUpdate().Model(&u).Where("id = ?", u.ID).Exec(context.Background())
	return
}
func (db *DB) CheckAuthToken(token string) (models.User, error) {
	at, err := db.GetAuthToken(token)
	if err != nil {
		return models.User{}, err
	}
	return *at.User, at.Validate()
}
