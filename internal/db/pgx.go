// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package db

import (
	"context"

	"github.com/jackc/pgx/v5"
)

func InitPGX(ctx context.Context, dsn string) (*pgx.Conn, error) {
	return pgx.Connect(ctx, dsn)
}
