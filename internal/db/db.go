// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package db

import (
	"context"
	"time"

	"codeberg.org/libreedu/common/log"

	"database/sql"

	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
)

func Init(logger *log.Logger, dsn string) (*bun.DB, error) {
	db := getDB(dsn)
	return db, await(logger, db)
}

func getDB(dsn string) *bun.DB {
	return bun.NewDB(sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn))), pgdialect.New())
}

// Function returns when the database connection is established or fails
func await(logger *log.Logger, db *bun.DB) error {
	var s time.Duration = 1 * time.Second
	ticker := time.NewTicker(s)
	ctx := context.Background()
	for {
		<-ticker.C
		var n int
		if err := db.NewSelect().ColumnExpr("15").Scan(ctx, &n); err != nil {
			logger.Warnf("PostgreSQL error: %s", err.Error())
			logger.Warnf("retrying in %d seconds", s/time.Second)
			ticker.Reset(s)
			s = s << 1
			continue
		}
		if n != 15 {
			panic("n != 15 from database")
		}
		logger.Info() <- "successfully connected to the database"
		return nil
	}
}
