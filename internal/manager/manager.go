// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package manager

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	soplugin "plugin"
	"slices"
	"time"

	"codeberg.org/libreedu/common/log"
	"codeberg.org/libreedu/hub/internal/db"
	. "codeberg.org/libreedu/hub/internal/types"
	"github.com/go-chi/chi/v5"
	"github.com/spf13/viper"
	"golang.org/x/exp/maps"
)

type Manager struct {
	Logger       *log.Logger
	DB           *DB
	Srv          *http.Server
	Router       *chi.Mux
	PluginParams PluginParams
	// A map of each name to plugin. This should probably be a
	// ``sync.Map``, because it will be accessed by
	// ``{ CrossPlugin; APIPlugin }`` HTTP handlers.
	Plugins map[string]Plugin
	// Plugins that implement CrossPlugin
	IPlugins []CrossPlugin

	DSN string
}

// M is whether this is the firstrun migrations
func (m *Manager) runPluginMigrations(p DatabasePlugin, M bool) error {
	logger := m.Logger
	if M {
		logger = log.New(io.Discard, "")
	}

	var latest time.Time
	if m.DB.NewRaw("select updated_at from migrations where plugin = ?;", p.Name()).Scan(context.Background(), &latest) != nil {
		latest = time.Unix(0, 0)
	}
	finalMigration := time.Unix(0, 0)
	migrations := p.Migrations()
	timestamps := maps.Keys(migrations)
	slices.Sort(timestamps)
	for _, k := range timestamps {
		v := migrations[k]
		logger.Debugf("checking %s migration %d", p.Name(), int64(k))
		if int64(k) >= latest.Unix() {
			if err := v(m.DB); err != nil {
				p.Destroy()
				logger.Errorf("failed to load plugin %s: database migrations failed: %s", p.Name(), err.Error())
				return err
			}
			finalMigration = time.Unix(int64(k)+1, 0)
			continue
		}
	}
	if finalMigration.Unix() != 0 {
		if _, err := m.DB.Exec("insert into migrations (plugin, updated_at) values (?, ?) on conflict (plugin) do update set updated_at = ?;", p.Name(), finalMigration, finalMigration); err != nil {
			logger.Errorf("failed to update migration status: %s", err.Error())
		}
	}

	return nil
}

func (m *Manager) AddPlugins(plugins ...Plugin) (int, error) {
	for i, p := range plugins {
		logger := log.New(os.Stderr, "plugins: "+p.Name())
		logger.DebugOut = m.Logger.DebugOut
		params := m.PluginParams
		if ch, err := os.Open("./config/" + p.Name() + ".conf"); err != nil {
			m.Logger.Warnf("failed to load config for plugin %s: %s", p.Name(), err.Error())
		} else {
			params.ConfigHandle = ch
			defer ch.Close()
		}
		params.Unstable = &unstableAPI{
			m: m,
			p: p,
		}
		if err := p.Init(logger, params); err != nil {
			m.Logger.Errorf("failed to load plugin %s: %s", p.Name(), err.Error())
			return i, err
		}
		m.Plugins[p.Name()] = p
		if p, ok := p.(APIPlugin); ok {
			m.Router.Group(p.Route)
		}
		if p, ok := p.(DatabasePlugin); ok {
			if err := m.runPluginMigrations(p, false); err != nil {
				return i, err
			}
		}
		if p, ok := p.(CrossPlugin); ok {
			m.IPlugins = append(m.IPlugins, p)
		}
		m.Logger.Infof("successfully loaded plugin %s", p.Name())
	}
	return -1, nil
}
func (m *Manager) AddPluginFile(filename string) error {
	so, err := soplugin.Open(filename)
	if err != nil {
		m.Logger.Errorf("failed to add plugin file %s: %s", filename, err.Error())
		return err
	}
	p, _ := so.Lookup("Plugin")
	po, ok := interface{}(p).(Plugin)
	if !ok {
		re := fmt.Errorf("failed to add plugin file %s: not a plugin file", filename)
		m.Logger.Error() <- re.Error()
		return re
	}
	_, err = m.AddPlugins(po)
	return err
}
func (m *Manager) GetPluginMap() map[string]Plugin { return m.Plugins }
func (m *Manager) Finalize() {
	for _, p := range m.IPlugins {
		p.Extend(m.Plugins)
	}
}
func (m *Manager) Listen(_srv *http.Server) {
	if _srv != nil {
		m.Srv = _srv
	}
	m.Srv.Handler = m.Router
	m.Logger.Info() <- "listening on " + m.Srv.Addr
	if err := m.Srv.ListenAndServe(); err != http.ErrServerClosed {
		m.Logger.Fatal() <- err.Error()
	}
}
func (m *Manager) Shutdown() error {
	for k, p := range m.Plugins {
		p.Destroy()
		delete(m.Plugins, k)
		m.Logger.Debugf("destroyed plugin %s", p.Name())
	}
	return m.Srv.Shutdown(context.Background())
}

func GetManager(logger *log.Logger, dsn string) (*Manager, error) {
	manager := &Manager{}
	manager.Logger = logger
	db, err := db.Init(log.New(os.Stderr, "db"), dsn)
	if err != nil {
		return nil, err
	}
	manager.DB = &DB{db}
	manager.PluginParams.DB = manager.DB
	manager.Srv = &http.Server{}
	manager.Router = chi.NewRouter()
	manager.Plugins = map[string]Plugin{}
	manager.IPlugins = []CrossPlugin{}

	manager.DSN = dsn

	err = firstRun(manager.DB)
	if err != nil {
		return manager, err
	}
	err = manager.runPluginMigrations(basePlugin, true)
	return manager, err
}

// Returns a Manager using “config/hub.yml“ to set up.
func GetDefaultManager(logger *log.Logger) (*Manager, error) {
	viper := viper.New()
	viper.SetConfigName("hub")
	viper.SetConfigType("yml")
	viper.AddConfigPath("./config")
	viper.SetDefault("PORT", "3000")
	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	manager := &Manager{}
	manager.Logger = logger
	manager.Logger.DebugOut = os.Getenv("ZCDEBUG") == "1"
	dsn := viper.GetString("DB_ADDR")
	db, err := db.Init(log.New(os.Stderr, "db"), dsn)
	if err != nil {
		return nil, err
	}
	manager.DB = &DB{db}
	manager.PluginParams.DB = manager.DB
	manager.Srv = &http.Server{Addr: ":" + viper.GetString("PORT")}
	manager.Router = chi.NewRouter()
	manager.Plugins = map[string]Plugin{}
	manager.IPlugins = []CrossPlugin{}

	manager.DSN = dsn

	err = firstRun(manager.DB)
	if err != nil {
		return manager, err
	}
	err = manager.runPluginMigrations(basePlugin, true)
	return manager, err
}
