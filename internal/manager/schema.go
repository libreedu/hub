// Copyright (c) 2023 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package manager

import (
	"context"
	_ "embed"

	"codeberg.org/libreedu/common/log"
	. "codeberg.org/libreedu/hub/internal/types"
)

//go:embed schema.sql
var schemasql string

func firstRun(db *DB) error {
	// There was ironically no migration support for the main schema.sql,
	// so here we skip if a table in it exists.
	var skip bool
	if err := db.NewRaw("select exists (select 1 from information_schema.tables where table_name = 'migrations' );").Scan(context.Background(), &skip); err != nil {
		return err
	} else if skip {
		return nil
	}
	_, err := db.Query(schemasql)
	return err
}

type basePluginT struct{}

func (basePluginT) Init(*log.Logger, PluginParams) error {
	return nil
}
func (basePluginT) Name() string { return "hub" }
func (basePluginT) Migrations() map[uint]func(*DB) error {
	return map[uint]func(*DB) error{
		0: func(db *DB) error { return nil },
		1685943688: func(db *DB) error {
			_, err := db.Query(`
      create index users_idx_username on users (username);
      create index users_idx_email    on users (email);
      `)
			return err
		},
	}
}
func (basePluginT) Destroy() {}

var basePlugin DatabasePlugin = basePluginT{}
