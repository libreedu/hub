-- Copyright (c) 2022 Alexander Noble
-- SPDX-License-Identifier: AGPL-3.0-or-later

create table migrations (
  plugin text primary key,
  updated_at timestamp not null -- the timestamp of the last migration,
                                -- plus one, NOT the time it was applied
);

create table users (
  id uuid primary key not null,
  username varchar(32) unique not null,

  given_name  varchar(50) not null,
  family_name varchar(50) not null,
  middle_name varchar(50),
  name_suffix varchar(3),
  email text, -- TODO: there should be further validation, either
              -- application-side or in the SQL.

  last_login timestamp,
  created_at timestamp default current_timestamp,
  updated_at timestamp
);

create table auth_tokens (
  token text primary key not null,
  uid uuid not null references users(id),
  revoked bool default FALSE,
  issued_at timestamp not null
);
