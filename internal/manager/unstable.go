// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package manager

import (
	"context"

	"codeberg.org/libreedu/hub/internal/db"
	"codeberg.org/libreedu/hub/internal/types"
	"github.com/jackc/pgx/v5"
)

type unstableAPI struct {
	m *Manager
	p types.Plugin
}

// Allocates a *pgx.Conn
func (u *unstableAPI) GetPgx() (*pgx.Conn, error) {
	return db.InitPGX(context.TODO(), u.m.DSN)
}

func (u *unstableAPI) GetResHandle() types.InternalHandle {
	return types.HandleFrom(u.m, u.p)
}
