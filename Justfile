# Copyright (c) 2022 Alexander Noble
# SPDX-License-Identifier: AGPL-3.0-or-later

dist:
  mkdir -p bin
  go build -o ./bin/dist -ldflags "-X codeberg.org/libreedu/hub.Version=$(git tag | tail -1)+$(git rev-list HEAD ^$(git tag | tail -1) | wc -l)-$(date '+%Y%m%d')-$(git rev-list --abbrev-commit -1 HEAD)" -trimpath -buildmode pie ./cmd/dist

plugin-preflight path:
  -cd '{{path}}' && just -f hubplugin.just preflight

plugin path: (plugin-preflight path)
  cd '{{path}}' && go build -o plugin.so -ldflags "-X codeberg.org/libreedu/hub.Version=$(git tag | tail -1)+$(git rev-list HEAD ^$(git tag | tail -1) | wc -l)-$(date '+%Y%m%d')-$(git rev-list --abbrev-commit -1 HEAD)" -trimpath -buildmode plugin .
