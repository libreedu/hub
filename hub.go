// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package hub

import (
	"net/http"

	"codeberg.org/libreedu/common/log"
	"codeberg.org/libreedu/hub/internal/manager"
)

type Manager interface {
	// On error, returns the index of the plugin erroring and the error
	// received, or ``(-1, nil)`` if all plugins initialized successfully.
	AddPlugins(...Plugin) (int, error)
	AddPluginFile(filename string) error
	GetPluginMap() map[string]Plugin
	// Finishishes initializing each plugin, specifically by passing the
	// plugin map to CrossPlugins.
	Finalize()
	// Starts listening. If the Server pointer is nil, Listen will use the
	// implementation default. Listen will not modify a passed Server
	// except for the Handler.
	Listen(*http.Server)
	// Shuts down the ``http.Server`` in Listen() and calls Destroy() on
	// each plugin. Returns any error in shutting down, for instance from
	// ``http.Server.Shutdown()``.
	Shutdown() error
}

var _ Manager = &manager.Manager{}

func GetDefaultManager(logger *log.Logger) (Manager, error) {
	return manager.GetDefaultManager(logger)
}
func GetManager(logger *log.Logger, dsn string) (Manager, error) {
	return manager.GetManager(logger, dsn)
}

var Version string
