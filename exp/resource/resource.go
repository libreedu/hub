// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// Package resource provides an alternate method of acquiring resources.
//
// All functions will panic if h is not from type internal/manager.Manager.
package resource

import (
	"context"
	"database/sql"
	"io"
	"os"

	"codeberg.org/libreedu/hub/internal/db"
	hm "codeberg.org/libreedu/hub/internal/manager"
	"codeberg.org/libreedu/hub/internal/types"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/spf13/viper"
)

type manager = hm.Manager
type handle interface {
	Manager() any
	Plugin() types.Plugin
}

func from(h types.InternalHandle) (*manager, types.Plugin) {
	return h.(handle).Manager().(*manager), h.(handle).Plugin()
}

type ConfigHandle struct{ io.ReadSeekCloser }
type ConfigProvider interface {
	ConfigHandle | *viper.Viper
}

// GetConfig returns the configuration for the plugin.
//
// A [ConfigHandle] is simply an [*os.File], but this is not an API
// guarantee.
func GetConfig[T ConfigProvider](h types.InternalHandle) (T, error) {
	m, p := from(h)
	_ = m

	switch any(*new(T)).(type) {
	case ConfigHandle:
		file, err := os.Open("./config/" + p.Name() + ".conf")
		return any(ConfigHandle{file}).(T), err

	case *viper.Viper:
		viper := viper.New()
		viper.WriteConfig()
		viper.SetConfigName(p.Name())
		viper.AddConfigPath("./config/")
		return any(viper).(T), nil
	}
	panic("unreachable")
}

type DatabaseProvider interface {
	*types.DB | *sql.DB | *pgx.Conn | *pgxpool.Pool
}

// GetDB returns a database interface that is safe to use within the
// plugin, insofar as it is guaranteed not to be used unsafely elsewhere.
//
// Currently, [*types.DB] and [*sql.DB] are shared between plugins, as they
// are pooled connections; and [*pgx.Conn] and [*pgxpool.Pool] are created
// on demand for the plugin requesting it.
//
// Thread safety is a property of the specific T requested; specifically,
// [*pgx.Conn] is not thread-safe.
func GetDB[T DatabaseProvider](h types.InternalHandle) (T, error) {
	m, p := from(h)
	_ = p

	switch any(*new(T)).(type) {
	case *types.DB:
		var db *types.DB = m.DB
		return any(db).(T), nil
	case *sql.DB:
		var db *sql.DB = m.DB.DB.DB
		return any(db).(T), nil
	case *pgx.Conn:
		conn, err := db.InitPGX(context.TODO(), m.DSN)
		var _ *pgx.Conn = conn
		return any(conn).(T), err
	case *pgxpool.Pool:
		pool, err := pgxpool.New(context.TODO(), m.DSN)
		var _ *pgxpool.Pool = pool
		return any(pool).(T), err
	}
	panic("unreachable")
}
