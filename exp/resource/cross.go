// Copyright (c) 2024 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package resource

import (
	"errors"
	"reflect"

	"codeberg.org/libreedu/hub/internal/types"
)

var ErrorPluginNotExist = errors.New("plugin does not exist")
var ErrorPluginIncorrectType = errors.New("plugin is incorrect type")

type errorIs struct {
	is  error
	str string
}

func (e errorIs) Is(target error) bool {
	return target == e.is
}
func (e errorIs) Error() string {
	return e.str
}

// Gets a plugin from the plugin map and checks if it fulfills the provided
// interface T. Note that if used in Plugin.Init, the other plugin might
// not be loaded yet. Use at runtime e.g. inside a route.
//
// Compare errors ErrorPluginNotExist and ErrorPluginIncorrectType with
// [errors.Is].
//
// Example:
//
//	h := pa.Unstable.GetResHandle()
//	avapi, err := resource.GetPlugin[avatar.AvatarPlugin](h, avatar.Plugin.Name())
//	if err != nil {
//		p.logger.Errorf("Failed to get avatar plugin: %s", err.Error())
//		return
//	}
//	url, err := avapi.XGetAvatarURL(context.Background(), "username")
//	if err != nil {
//		p.logger.Errorf("Could not fetch avatar: %s", err.Error())
//		return
//	}
//	p.logger.Infof("URL: %s", url)
func GetPlugin[T interface{ types.Plugin }](h types.InternalHandle, name string) (T, error) {
	m, p := from(h)
	_ = p

	if Pl, ok := m.Plugins[name]; !ok {
		return *new(T), errorIs{ErrorPluginNotExist, "plugin " + name + " does not exist"}
	} else if Pt, ok := Pl.(T); !ok {
		Type := reflect.TypeOf(*new(T))
		if Type == nil {
			panic("Type extends Plugin cannot be nil")
		}
		return *new(T), errorIs{ErrorPluginIncorrectType, "plugin " + name + " does not implement " + Type.Name()}
	} else {
		return Pt, nil
	}
}

func GetPluginMap(h types.InternalHandle) map[string]types.Plugin {
	m, _ := from(h)
	return m.Plugins
}
