// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package models

import (
	"errors"
	"time"

	"github.com/uptrace/bun"
)

type AuthToken struct {
	bun.BaseModel `bun:"table:auth_tokens"`
	Token         string `bun:",pk"`
	UID           string
	User          *User `bun:"-"`
	Revoked       bool
	IssuedAt      time.Time
}

// Unstable; these errors will probably move elsewhere.
var (
	ErrAuthTokenInvalid = errors.New("auth token issued or read from database incorrectly")
	ErrAuthTokenExpired = errors.New("auth token expired or revoked")
)

func (at AuthToken) Validate() error {
	if at.User.ID != at.UID {
		return ErrAuthTokenInvalid
	}
	if at.Revoked || (at.IssuedAt.Add(7 * 24 * time.Hour).Before(time.Now())) {
		return ErrAuthTokenExpired
	}
	return nil
}
