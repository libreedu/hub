// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// The `models` module provides core database types like User.
//
// These structures should stay pretty much the same, so if you need
// additional fields, just extend the struct and build the object with
// joins.
//
// Please don't alter tables; add a relationship with an ORM to another
// table, or use an adjacent table with joins:
// “select * from users join cldata.users on public.users.id = cldata.users.uid where id = $(uuidgen);“
package models
