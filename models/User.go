// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

package models

import (
	"time"

	"github.com/uptrace/bun"
)

type User struct {
	bun.BaseModel `bun:"table:users"`
	ID            string `json:"id" bun:",pk,type:uuid"`
	Username      string `json:"username"`

	GivenName  string `json:"given_name"`
	FamilyName string `json:"family_name"`
	MiddleName string `json:"middle_name,omitempty"`
	NameSuffix string `json:"name_suffix,omitempty"`
	Email      string `json:"email,omitempty"`

	LastLogin time.Time `json:"last_login"` // omitempty doesn't work for literal null
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
