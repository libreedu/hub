// Copyright (c) 2022 Alexander Noble
// SPDX-License-Identifier: AGPL-3.0-or-later

// Currently this package only contains the definitions for various
// plugin types. It is planned to contain a plugin manager interface and
// an implementation of one to make simple "startup scripts" listing
// plugins.
//
// For documentation of plugin types, see the package
// codeberg.org/libreedu/hub/internal/types
package hub
